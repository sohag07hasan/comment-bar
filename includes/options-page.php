<?php 
	global $commentbar;
	//saving the form
	if($_POST['comment_bar_save'] == 'y'){
		$commentbar->options->save_options($_POST['commentbar']);
	}
		
	$options = $commentbar->options->get_options();
	
	//var_dump($options);
		
?>

<style>
	div.commbentbar table td{
		padding-left: 20px;
	}
	
	div.commbentbar textarea{
		width: 500px;
		height: 100px;
	}
	
	div.commbentbar input{
		width: 500px;
		height: 30px;
	}
	
	div#commentbar_preview{	
		width: 90%;
		height: 155px;
		<?php echo stripslashes($options['css']); ?>		
	}
		
</style>


<div class="wrap">

	<h2>Comment Bar</h2>

	<form action="" method="post">
		<input type="hidden" name="comment_bar_save" value="y" />
	
		<div class="commbentbar styling">
			<table class="">
				<tr>
					<td><label for="commentbar_css"><h4>Css</h4></label></td>
					<td>
					<textarea name="commentbar[css]" id="commentbar_css"><?php echo stripslashes($options['css']); ?></textarea>
					<p>For gradient Css, Please <a href="<?php echo $commentbar->options->get_gradient_url(); ?>" target="_blank" >click</a></p>
					</td>
				</tr>
			
				<tr>
					<td><label for="commentbar_autoresponder"><h4>Autoresponder</h4></label></td>
					<td><textarea name="commentbar[autoresponder]" id="commentbar_autoresponder"><?php echo $options['autoresponder']; ?></textarea></td>
				</tr>
				
				<tr>
					<td><label for="commentbar_required_text"><h4>Reqired Text</h4></label></td>
					<td><textarea name="commentbar[required_text]" id="commentbar_required_text"><?php echo $options['required_text']; ?></textarea></td>
				</tr>
			
				<tr>
					<td><label for="commentbar_thumbnail"><h4>Thumbnail</h4></label></td>
					<td class="upload">
						<input value="<?php echo $options['thumbnail']; ?>" type="text" name="commentbar[thumbnail]" class="commentbar_thumbnail"> 
						 <input style="width: 130px;" type='button' class='button button-secondary button-upload' value='Upload an image'/></br>
						<img style='max-width: 150px; max-height: 150px; display:block' src='<?php echo $options['thumbnail']; ?>' class='preview-upload'/>
					</td>
				</tr>
			
				<tr>
					<td><label for="commentbar_affiliate"><h4>Affiliate</h4></label></td>
					<td><input value="<?php echo $options['affiliate']; ?>" type="text" name="commentbar[affiliate]" id="commentbar_affiliate"></td>
				</tr>
			</table>
		</div>
	
		<p>
			<input type="button" value="Preview" class="button button-secondary" /> &nbsp; &nbsp;
			<input type="submit" value="Save" class="button button-primary" />
		</p>	
		
	</form>
	
	<!--  cooment bar preview -->
	<!-- <div class="commentbar_preview_holder"> -->
		<div id="commentbar_preview">
			<div class="comment_bar_thumb_container inside_commentbar"> 
				<img class="commbent_bar_thumb" src="<?php echo $options['thumbnail'];  ?>" />
			</div>
			<div class="comment_bar_required_text_container inside_commentbar">
				<p><?php echo $options['required_text']; ?></p>
				<p>Name: <input type="text" >, Email: <input type="text"> <span>Yes! I want to get gift <input type="checkbox" checked /></span></p>
				
				<p>Comment: <textarea rows="1" cols="50" class="comment-box"></textarea>&nbsp; 		
				<input type="submit" value="Submit"> Powered by Commentbar! </p>
			</div>
		</div>
	<!-- </div>  -->
	
</div>