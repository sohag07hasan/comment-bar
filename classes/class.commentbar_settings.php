<?php 
/*
 * Settings for both Post wise or Gerneral
 * */

class CommentBarSettings{
	
	private $options = array('css', 'autoresponder', 'required_text', 'affiliate', 'thumbnail');
	private $gradient_url = "http://www.colorzilla.com/gradient-editor/";
	
	function __construct(){
		add_action('admin_menu', array(&$this, 'admin_menu'));
		
		//add metaboxex
		add_action( 'add_meta_boxes', array(&$this, 'add_meta_boxes' ));
		
		//save meta box info
		add_action('save_post', array(&$this, 'save_meta_boxes'), 10, 2);
		
		//necessary scripts for default media uploader
		add_action('admin_enqueue_scripts', array(&$this, 'admin_enqueue_scripts'));
	}
	
	
	//add option page
	function admin_menu(){
		add_options_page( 'Comment Bar Options', 'Comment Bar', 'manage_options', 'comment_bar', array(&$this, 'options_page'));
	}
	
	
	//options page content
	function options_page(){
		global $commentbar;
		include $commentbar->get_this_dir() . 'includes/options-page.php';
	}
	
	//save options
	function save_options($posted){
		$option_to_save = array();
		foreach($this->options as $option){
			if(isset($posted[$option])){
				$option_to_save[$option] = trim($posted[$option]);
			}
		}
		
		//var_dump($posted);
		//var_dump($option_to_save);
		
		update_option("commentbar_options", $option_to_save);
	}
	
	//get options
	function get_options(){
		$options = get_option("commentbar_options");
		return empty($options) ? array() : $options;
	}
	
	//get colore gredient url
	function get_gradient_url(){
		return $this->gradient_url;
	}
	
	//add some metaboxes
	function add_meta_boxes(){
		$post_types = array('post', 'page');
		foreach($post_types as $post_type){
			add_meta_box( 'Comment Bar | Post Options', __( 'Comment Bar', 'commentbar' ), array(&$this, 'commentbar_options_for_posts'), $post_type, 'side', 'high' );
		}
	}
	
	//comment bar option for page
	function commentbar_options_for_posts($post){
		global $commentbar;
		include $commentbar->get_this_dir() . 'includes/metaboxes/per-page.php';
	}
	
	//svae the meta boxes
	function save_meta_boxes($post_id, $post){
		if(isset($_POST['commentbar_activate'])){
			update_post_meta($post_id, '_commentbar_activate', "1");
		}
		else{
			update_post_meta($post_id, '_commentbar_activate', "0");
		}
		
		if(isset($_POST['commentbar_timedealy'])){
			update_post_meta($post_id, '_commentbar_timedealy', $_POST['commentbar_timedealy']);
		}		
	}
	
	//boolean to check if comment bar is active
	function is_commentbar_active($post_id){
		return (get_post_meta($post_id, '_commentbar_activate', true) == "1") ? true : false;  
	}
	
	
	//return the time delay in seconds when the comment bar pops up
	function get_time_delay($post_id){
		$default = 30;
		$delay = get_post_meta($post_id, '_commentbar_timedealy', true);
		
		return empty($delay) ? $default : (int) $delay;
	}
	
	
	//scripts to use default media uploader
	function admin_enqueue_scripts(){
		global $commentbar;
		
		if($_GET['page'] == 'comment_bar'){
			wp_enqueue_script('jquery');
			wp_enqueue_style( 'thickbox' ); // Stylesheet used by Thickbox
			wp_enqueue_script( 'thickbox' );
			wp_enqueue_script( 'media-upload' );
			wp_register_script('commentbar-media-upload', $commentbar->get_this_url() . 'js/plugin.media-uploader.js', array('jquery', 'thickbox', 'media-upload'));
			wp_enqueue_script('commentbar-media-upload');
						
			wp_register_style('comment-bar-preview-styling', $commentbar->get_this_url() . 'css/comment-bar-preview.css');
			wp_enqueue_style('comment-bar-preview-styling');
		}
	}
	
}


?>