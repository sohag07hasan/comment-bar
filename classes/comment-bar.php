<?php 
/*
 * This class is the controller of the every class
 * */

class CommentBar{
	
	public $options;
	public $postview;
	
	//constructor
	function __construct(){
		include $this->get_this_dir() . 'classes/class.commentbar_settings.php';
		$this->options = new CommentBarSettings();
		
		include $this->get_this_dir() . 'classes/class.post-view.php';
		$this->postview = new CommentBarFrontEnd();
	}
	
	//get base directory of the current plugin
	function get_this_dir(){
		return COMMENTBAR_DIR;
	}

	//get current plugin's base url
	function get_this_url(){
		return COMMENTBAR_URI;
	}
	
}

?>