<?php 

class CommentBarFrontEnd{
	
	function __construct(){
		add_action('wp_footer', array(&$this, 'include_comment_bar'));
		add_action('wp_enqueue_scripts', array(&$this, 'include_scripts'));
		//add_filter('the_content', array(&$this, 'add_comment_bar'));
	}
	
	
	//include the comment bar in footer sectio n
	function include_comment_bar(){
		if(is_page() || is_single()):
			global $commentbar;
			include $commentbar->get_this_dir() . 'includes/front-end.php';
		endif;
	}
	
	
	//include scripts
	function include_scripts(){
		if(is_page() || is_single()):
			global $commentbar;
			wp_register_style('commentbar-front-end-css', $commentbar->get_this_url() . 'css/front-end.css');
			wp_enqueue_style('commentbar-front-end-css');
		endif;
	}
	
	//add comment bar
	function add_comment_bar($content){
		if(is_page() || is_single()){
			global $commentbar;
			ob_start();
			include $commentbar->get_this_dir() . 'includes/front-end.php';
			$com = ob_get_contents();
			ob_end_flush();
			$content .= $com;
		}
		
		return $content;
	}
}

?>
